# errors

[![build status](https://gitlab.com/thomaslindstr_m/errors/badges/master/build.svg)](https://gitlab.com/thomaslindstr_m/errors/commits/master)

easily create meaningful errors

```
npm install @amphibian/errors
```

```javascript
var errors = require('@amphibian/errors');

try {
    throw errors.invalidInput('invalid_email_address', 'email', 't@hom.as');
} catch (error) {
    console.log(error);

    /*
    {
        [Error: Invalid Input (invalid_email_address): email t@hom.as]
            status: 400,
            type: 'invalid_input',
            code: 'invalid_email_address',
            data: ['email', 't@hom.as']
    }
    */
}
```

## Create your own error

```javascript
var myError = errors.newError('My Error', {
    status: 400,
    type: 'my_error',
    class: TypeError,
    stack: true
});

try {
    throw myError('my_error_code', 'Some description', 382763);
} catch (error) {
    console.log(error);

    /*
    {
        TypeError: My Error (my_error_code): Some description 382763
            at [[... stack trace ...]]

            status: 400,
            type: 'my_error',
            code: 'my_error_code',
            data: ['Some description', 'Some more information here']
    }
    */
}
```

## Built-in error types

### `notFound`: Not Found
```javascript
{status: 404, type: 'not_found'}
```

### `methodNotAllowed`: Method Not Allowed
```javascript
{status: 405, type: 'method_not_allowed'}
```

### `missingRequiredParameters`: Missing Required Parameters
```javascript
{status: 400, type: 'missing_required_parameters'}
```

### `invalidInput`: Invalid Input
```javascript
{status: 400, type: 'invalid_input'}
```

### `typeError`: Type Error
```javascript
{status: 400, type: 'type_error', class: TypeError}
```

### `unauthorized`: Unauthorized
```javascript
{status: 401, type: 'unauthorized'}
```

### `rateLimitExceeded`: Rate Limit Exceeded
```javascript
{status: 429, type: 'rate_limit_exceeded'}
```

### `fatalError`: Fatal Error
```javascript
{status: 500, type: 'fatal_error', stack: true}
```
