// ---------------------------------------------------------------------------
//  test.js
// ---------------------------------------------------------------------------

    var errors = require('./index.js');
    var assert = require('assert');

    describe('errors', function () {
        it('message get a colon appended when no code is set', () => {
            var error = errors.newError('My error')(null, 'test');
            assert.equal(error.message, 'My error: test');
        });

        it('code get a colon appended when data is set', () => {
            var error = errors.newError('My error', {type: 'my_code'})(null, 'test');
            assert.equal(error.message, 'My error (my_code): test');
        });

        it('separate error data with spaces when more are provided', () => {
            var error = errors.newError('My error')(null, 'test', 'something');
            assert.equal(error.message, 'My error: test something');
        });

        it('missing_required_parameters with data', () => {
            var error = errors.missingRequiredParameters(null, 'email');

            assert.equal(error.code, 'missing_required_parameters');
            assert.equal(error.status, 400);
            assert.equal(error.message, 'Missing Required Parameters (missing_required_parameters): email');
            assert.equal(error.data[0], 'email');
        });

        it('missing_required_parameters with custom code', () => {
            var error = errors.missingRequiredParameters('missing_email', 'email');

            assert.equal(error.code, 'missing_email');
            assert.equal(error.type, 'missing_required_parameters');
            assert.equal(error.status, 400);
            assert.equal(error.message, 'Missing Required Parameters (missing_email): email');
            assert.equal(error.data[0], 'email');
        });

        it('not_found with no data', () => {
            var error = errors.notFound();

            assert.equal(error.code, 'not_found');
            assert.equal(error.status, 404);
            assert.equal(error.message, 'Not Found (not_found)');
        });

        it('method_not_allowed with no data', () => {
            var error = errors.methodNotAllowed();

            assert.equal(error.code, 'method_not_allowed');
            assert.equal(error.status, 405);
            assert.equal(error.message, 'Method Not Allowed (method_not_allowed)');
        });

        it('missing_required_parameters with no data', () => {
            var error = errors.missingRequiredParameters();

            assert.equal(error.code, 'missing_required_parameters');
            assert.equal(error.status, 400);
            assert.equal(error.message, 'Missing Required Parameters (missing_required_parameters)');
        });

        it('invalid_input with no data', () => {
            var error = errors.invalidInput();

            assert.equal(error.code, 'invalid_input');
            assert.equal(error.status, 400);
            assert.equal(error.message, 'Invalid Input (invalid_input)');
        });

        it('type_error with no data', () => {
            var error = errors.typeError();

            assert.equal(error.code, 'type_error');
            assert.equal(error.status, 400);
            assert.equal(error.message, 'Type Error (type_error)');
        });

        it('type_error should be instance of TypeError', () => {
            var error = errors.typeError();
            assert.equal(error instanceof TypeError, true);
        });

        it('unauthorized with no data', () => {
            var error = errors.unauthorized();

            assert.equal(error.code, 'unauthorized');
            assert.equal(error.status, 401);
            assert.equal(error.message, 'Unauthorized (unauthorized)');
        });

        it('rate_limit_exceeded with no data', () => {
            var error = errors.rateLimitExceeded();

            assert.equal(error.code, 'rate_limit_exceeded');
            assert.equal(error.status, 429);
            assert.equal(error.message, 'Rate Limit Exceeded (rate_limit_exceeded)');
        });

        it('fatal_error with no data', () => {
            var error = errors.fatalError();

            assert.equal(error.code, 'fatal_error');
            assert.equal(error.status, 500);
            assert.equal(error.message, 'Fatal Error (fatal_error)');
        });
    });
