// ---------------------------------------------------------------------------
//  errors
//  easily create meaningful errors
// ---------------------------------------------------------------------------

var objectHasProperty = require('@amphibian/object-has-property');

/**
 * Create a new error
 * @param {string} prefix
 * @param {object} information
 *
 * @returns {function} error creation function
**/
function newError(prefix, information) {
    information = information || {};
    var Constructor = information.class || Error;

    return function (code) {
        var data = ((arguments.length === 1)
            ? [arguments[0]]
            : Array.apply(null, arguments)
        ).slice(1);

        var dataLength = data.length;
        var errorCode = code || information.type;
        var errorMessageFragments = [];

        if (prefix) {
            if (errorCode || (dataLength === 0)) {
                errorMessageFragments.push(prefix);
            } else {
                errorMessageFragments.push(prefix + ':');
            }
        }

        if (errorCode) {
            if (dataLength === 0) {
                errorMessageFragments.push('(' + errorCode + ')');
            } else {
                errorMessageFragments.push('(' + errorCode + '):');
            }
        }

        if (dataLength > 0) {
            errorMessageFragments.push(data.join(' '));
        }

        var error = new Constructor(errorMessageFragments.join(' '));

        error.status = information.status || 400;
        error.type = information.type || 'error';
        error.code = errorCode;
        error.data = data || [];

        if (information.stack !== true) {
            error.stack = null;
        }

        return error;
    };
}

var errors = {
    newError: newError,

    notFound: newError('Not Found', {
        status: 404,
        type: 'not_found'
    }),
    methodNotAllowed: newError('Method Not Allowed', {
        status: 405,
        type: 'method_not_allowed'
    }),
    missingRequiredParameters: newError('Missing Required Parameters', {
        status: 400,
        type: 'missing_required_parameters'
    }),
    invalidInput: newError('Invalid Input', {
        status: 400,
        type: 'invalid_input'
    }),
    typeError: newError('Type Error', {
        status: 400,
        type: 'type_error',
        class: TypeError
    }),
    unauthorized: newError('Unauthorized', {
        status: 401,
        type: 'unauthorized'
    }),
    rateLimitExceeded: newError('Rate Limit Exceeded', {
        status: 429,
        type: 'rate_limit_exceeded'
    }),
    fatalError: newError('Fatal Error', {
        status: 500,
        type: 'fatal_error',
        stack: true
    })
};

module.exports = errors;
